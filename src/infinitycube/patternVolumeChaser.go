package main

import (
	"math"
)

type VolumeChaser struct {
	Effect
	sound      *ProcessedAudio
	travelTime float64   // [s] time the running lieghts need to last led
	ledCount   int       // nr of leds the the amplitudes travel
	even       bool      // save if nr of are even or odd
	amplitudes []float64 // history of amplitudes of the sound signal
	blockSize  int       // nr of amplitudes averaged to calculate brightness of a single led
	mirrored   bool      // decide if effect is mirrored to center led
	filling    bool      // decide if effect fills entire cube
}

func NewVolumeChaser(disp Display, cg ColorGenerator, s *ProcessedAudio, travelTime float64, travelDistance int, mirrored, cubeFilling bool) *VolumeChaser {
	ef := NewEffect(disp, 0.5, 0.0)

	// calculate duration of a single frame
	d := 1.0 / float64(fpsTarget)
	// calculate how many frames need to be saved to represent travelTime
	len := int(math.Ceil(travelTime / d))

	v := &VolumeChaser{
		Effect:     ef,
		sound:      s,
		ledCount:   travelDistance,
		amplitudes: make([]float64, len),
		blockSize:  len / travelDistance,
		mirrored:   mirrored,
		filling:    cubeFilling,
	}

	if v.mirrored {
		// check if nr of leds is odd or even
		if v.ledCount%2 == 0 {
			v.even = true
		} else {
			v.even = false
		}
		// only calc half the leds if effect is mirrored
		v.ledCount = v.ledCount / 2
	}

	// set color generator
	v.Painter = cg

	return v
}

func (s *VolumeChaser) Update() {

	// shift all values in amplitude history one to the back
	for i := len(s.amplitudes) - 2; i >= 0; i-- {
		s.amplitudes[i+1] = s.amplitudes[i]
	}

	// add current amplitude to history
	s.amplitudes[0] = s.sound.currentVolume - s.sound.averageVolume

	// calculate brightness of every led
	for i := 0; i < s.ledCount; i++ {
		val := 0.0
		start := i * s.blockSize
		// average amplitudes of one block
		for p := start; p < start+s.blockSize; p++ {
			val += s.amplitudes[p]
		}
		val = val / float64(s.blockSize)

		if s.mirrored {
			// with even nr of leds their have to be two center leds
			// with odd nr of leds only on center led is needed
			off := 0
			if s.even {
				off = 1
			}

			s.Leds[s.OffsetPar+s.ledCount+i].V = val
			s.Leds[s.OffsetPar+s.ledCount-i-off].V = val

		} else {
			s.Leds[s.OffsetPar+i].V = val
		}
	}

	// copy single chaser to the entire cube
	if s.filling {
		n := 1
		if s.mirrored {
			n = 2
		}

		for i := s.OffsetPar + n*s.ledCount; i < s.myDisplay.NrOfLeds(); i++ {
			p := s.OffsetPar + i%(n*s.ledCount)
			s.Leds[i] = s.Leds[p]
		}
	}
	// every update function of an effect ends with this snippet
	s.Painter.Update()
	s.Leds = s.Painter.Colorize(s.Leds)
	s.myDisplay.AddEffect(s.Effect)
}
