// patternSparkle.go
package main

import (
	"math/rand"
)

type StarDust struct {
	Effect
	sound    *ProcessedAudio
	ledVs    []float64
	Chance   float64
	Cooldown float64
	reactive bool
	filling  bool
}

func NewStarDust(disp Display, cg ColorGenerator, s *ProcessedAudio, length int, audioReactive bool, filling bool) *StarDust {
	ef := NewEffect(disp, 0.5, 0.0)

	e := &StarDust{
		Effect:   ef,
		sound:    s,
		ledVs:    make([]float64, len(ef.Leds)),
		Chance:   0.006,
		Cooldown: 2 / float64(fpsTarget),
		reactive: audioReactive,
		filling:  filling,
	}

	e.LengthPar = length
	e.Painter = cg
	return e
}

func (e *StarDust) Update() {
	for i := (0 + e.OffsetPar); i < (e.OffsetPar + e.LengthPar); i++ {
		if e.ledVs[i] > 0 {

			nV := e.ledVs[i] - e.Cooldown

			if nV < 1e-3 {
				nV = 0
			}

			e.ledVs[i] = nV
			e.Leds[i].V = e.ledVs[i]

		} else {
			// adjust chance a led lights up
			if e.reactive {
				e.Chance = (e.sound.currentVolume - e.sound.averageVolume) / 5.0
			}

			val := rand.Float64()
			if val < e.Chance {
				e.ledVs[i] = rand.Float64()
				e.Leds[i].V = e.ledVs[i]
			}
		}

		// copy single chaser to the entire cube
		if e.filling {
			for i := e.OffsetPar + e.LengthPar; i < e.myDisplay.NrOfLeds(); i++ {
				p := e.OffsetPar + i%e.LengthPar
				e.Leds[i] = e.Leds[p]
			}
		}

	}

	// every update function of an effect ends with this snippet
	e.Painter.Update()
	e.Leds = e.Painter.Colorize(e.Leds)
	e.myDisplay.AddEffect(e.Effect)
}
