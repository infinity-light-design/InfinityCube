package main

import (
	"time"
)

func GreenBinaryWheel(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	cc1 := NewConstantColor(0.8, 130)
	sd := NewBinaryWheel(eH.myDisplay, cc1, EDGE_LENGTH, 300*time.Millisecond)

	effectMap := map[Effector]time.Duration{}
	effectMap[sd] = playTime

	return effectMap
}

func GoldenStarDust(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	cc1 := NewConstantColor(0.20, 47)
	sd := NewStarDust(eH.myDisplay, cc1, eH.audio, EDGES_PER_SIDE*EDGE_LENGTH, true, true)
	sd.Cooldown = 4.0 / float64(fpsTarget)
	effectMap := map[Effector]time.Duration{}
	effectMap[sd] = playTime

	return effectMap
}

func PurpleStarDust(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	cc1 := NewConstantColor(1, 310)
	ca := NewCellularAutomata(eH.myDisplay, cc1)
	ca.Value = 0.2
	ca.SetSecsPerGen(0.3)

	rP := NewRebornPainter(95, false)
	sd := NewStarDust(eH.myDisplay, rP, eH.audio, EDGES_PER_SIDE*EDGE_LENGTH, true, true)

	effectMap := map[Effector]time.Duration{}
	effectMap[sd] = playTime
	effectMap[ca] = playTime

	return effectMap
}

func StaticWhite(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	cc1 := NewConstantColor(0, 0)
	solid := NewSolidBrightness(eH.myDisplay, cc1, 1.0)

	effectMap := map[Effector]time.Duration{}
	effectMap[solid] = playTime

	return effectMap
}

func RedSunsetStarDust(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {

	orange := Color{R: 50, G: 40, B: 10} //(53, 92, 125)
	redish := Color{R: 192, G: 30, B: 0} // (192, 108, 132)

	colors := make([]Color, 3)
	colors[0] = redish
	colors[1] = orange
	colors[2] = redish

	colGrad := NewColorGradient(colors, EDGE_LENGTH)
	ca := NewCellularAutomata(eH.myDisplay, colGrad)

	cc1 := NewConstantColor(.8, 35)
	sd := NewStarDust(eH.myDisplay, cc1, eH.audio, EDGES_PER_SIDE*EDGE_LENGTH, false, true)

	effectMap := map[Effector]time.Duration{}
	effectMap[ca] = playTime
	effectMap[sd] = playTime

	return effectMap
}

func MagmaPlasma(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	// sine 1
	cc1 := NewConstantColor(1, 0)
	sine1 := NewSine(eH.myDisplay, cc1, 0.3)
	sine1.Frequency = 2 * NR_OF_SIDES
	sine1.SetLoopTime(5 * NR_OF_SIDES)

	// sine 2
	cc2 := NewConstantColor(1, 40)
	sine2 := NewSine(eH.myDisplay, cc2, 0.4)
	sine2.Frequency = 3 * NR_OF_SIDES
	sine2.SetLoopTime(7 * NR_OF_SIDES)

	// multi running light
	//cc3 := NewConstantColor(1, 30)
	//mrl := NewMultiRunningLight(eH.myDisplay, cc3)

	magmaPlasma := map[Effector]time.Duration{}

	magmaPlasma[sine1] = playTime
	magmaPlasma[sine2] = playTime
	//magmaPlasma[mrl] = playTime

	return magmaPlasma
}

func CellularAutomataMonochrome(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	cc1 := NewConstantColor(1, 0)
	ca := NewCellularAutomata(eH.myDisplay, cc1)

	automata := map[Effector]time.Duration{}
	automata[ca] = playTime

	return automata
}

func CellularAutomataGradient(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	green := Color{R: 0, G: 90, B: 0}
	orange := Color{R: 0, G: 0, B: 50}

	colors := make([]Color, 3)
	colors[0] = orange
	colors[1] = green
	colors[2] = orange

	colGrad := NewColorGradient(colors, 2*EDGE_LENGTH)
	ca := NewCellularAutomata(eH.myDisplay, colGrad)

	automata := map[Effector]time.Duration{}
	automata[ca] = playTime

	return automata
}

func LinearSpectrumMonochrome(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	cc1 := NewConstantColor(1, 0)
	spec := NewLinearSpectrum(eH.myDisplay, cc1, eH.audio)

	effectMap := map[Effector]time.Duration{}
	effectMap[spec] = playTime

	return effectMap
}

func LinearEdgeSpectrumMonochrome(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	cc1 := NewConstantColor(0, 0)
	spec := NewLinearEdgeSpectrum(eH.myDisplay, cc1, eH.audio)

	effectMap := map[Effector]time.Duration{}
	effectMap[spec] = playTime

	return effectMap
}

func EdgeVolumeRedGreen(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {

	green := Color{R: 0, G: 255, B: 0}
	red := Color{R: 255, G: 0, B: 0}

	colors := make([]Color, 2)
	colors[0] = green
	colors[1] = red

	colGrad := NewColorGradient(colors, EDGE_LENGTH)
	vol := NewEdgeVolume(eH.myDisplay, colGrad, eH.audio)

	effectMap := map[Effector]time.Duration{}
	effectMap[vol] = playTime

	return effectMap
}

func MultiRunningLightHSV(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	hsv := NewHsvFade(60, 0)
	mrl := NewMultiRunningLight(eH.myDisplay, hsv)

	effectMap := map[Effector]time.Duration{}
	effectMap[mrl] = playTime

	return effectMap
}

func VolumeChaseMonochrome(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	//blue := Color{R: 0, G: 0, B: 255}
	//cc1 := NewConstantColor(1, 0)
	green := Color{R: 0, G: 90, B: 0}
	orange := Color{R: 0, G: 0, B: 50}

	colors := make([]Color, 3)
	colors[0] = orange
	colors[1] = green
	colors[2] = orange

	colGrad := NewColorGradient(colors, 2*EDGE_LENGTH)

	vc1 := NewVolumeChaser(eH.myDisplay, colGrad, eH.audio, 1, 2*EDGE_LENGTH, true, true)
	effectMap := map[Effector]time.Duration{}
	effectMap[vc1] = playTime

	return effectMap
}

func VolumeChaseFading(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	rP := NewRebornPainter(60, false)
	vc1 := NewVolumeChaser(eH.myDisplay, rP, eH.audio, 1, 2*EDGE_LENGTH, true, true)
	effectMap := map[Effector]time.Duration{}
	effectMap[vc1] = playTime

	return effectMap
}

func VolumeChaseBig(eH *EffectHandler, playTime time.Duration) map[Effector]time.Duration {
	// sine 2
	cc2 := NewConstantColor(1, 40)
	sine2 := NewSine(eH.myDisplay, cc2, 0.2)
	sine2.Frequency = 3 * NR_OF_SIDES
	sine2.SetLoopTime(7 * NR_OF_SIDES)

	cc1 := NewConstantColor(1, 0)
	vc1 := NewVolumeChaser(eH.myDisplay, cc1, eH.audio, 1, 2*EDGE_LENGTH, true, true)

	effectMap := map[Effector]time.Duration{}
	effectMap[sine2] = playTime
	effectMap[vc1] = playTime

	return effectMap
}
