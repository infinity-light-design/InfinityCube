package main

import (
	//"fmt"
	"log"
	"time"
	"github.com/kellydunn/go-opc"
)

type OutputHandler struct {
	opcClient *opc.Client
	input chan []Color
	recycle chan []Color
	loopTime time.Duration
	withDisplay bool
}

func NewOutputHandler(opcClientAdress string, input, recycle chan []Color, fps int) ( err error) {
	oc := opc.NewClient()
	err = oc.Connect("tcp", opcClientAdress)

	c := &OutputHandler{
		opcClient: oc,
		input: input,
		recycle: recycle,
		loopTime: time.Duration(float64(1000000000) / float64(fps)), // in nanoseconds
		withDisplay: true,
		
	}

	if err != nil {
		// if connection failed run without display
		c.withDisplay = false
	}

	for {
		c.Show()
	}
	
}

func (c *OutputHandler) Show() {
	loopStart := time.Now()

	// read color slice from channel and return slice afterwards
	//fmt.Println("dispatch:", len(c.input), "recycle:", len(c.recycle))

	leds := <- c.input
	defer func() {c.recycle <- leds}()

	if !c.withDisplay {
		return	
	}

	// send pixel data
	m := opc.NewMessage(0)
	m.SetLength(uint16(len(leds) * 3)) // *3 -> r, g, b
	//fmt.Println(leds)

	for i, led := range leds {
		m.SetPixelColor(i, led.R, led.G, led.B)
	}

	if c.opcClient != nil {
		err := c.opcClient.Send(m)
		if err != nil {
			log.Println("couldn't send color", err)
		}
	}

	if time.Since(loopStart) < (c.loopTime) {
		time.Sleep(c.loopTime - time.Since(loopStart))
	}

}
