$ = jQuery

document.ready = ->
  $("#PlayAllEffects").on('click', requestPlaylist)
  $("#PlayReactive").on('click', requestPlaylist)
  $("#PlayNonReactive").on('click', requestPlaylist)
  $("#Off").on('click', requestPlaylist)
  #$.get('status', (data) ->
  #  fillEffectSelector(data)
  #, 'json')

fillEffectSelector = (data) ->
  opt = ""
  for i in data['AvailableEffects']
    opt += "<option value='" + i + "'>" + i + "</option>"

  $('#effects').html("")
  $('#effects').append(opt)


requestPlaylist = (btn) ->
    effect = btn.currentTarget.id
    $.post('toggle', {t: effect}, (data, textStatus, jqXHR) -> 'json')
    console.log(effect)
