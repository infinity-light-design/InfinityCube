import csv
import matplotlib.pyplot as plt

t_start = 0

effectLoop = []
t_effectLoop = []

gettingSoundSignal = []
t_gettingSoundSignal = []

with open('timing.txt', newline='') as csvfile:
    timings = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in timings:

        # safe start time in first run
        if t_start == 0:
            t_start = int(row[1])

        loop_time_ms = int(row[2]) / 1000000  # nano to milli
        timestamp_ms = (int(row[1]) - t_start) / 1000000  # nano to milli

        if row[0] == "effectLoop":
            effectLoop.append(loop_time_ms)
            t_effectLoop.append(timestamp_ms)

        if row[0] == "gettingSoundSignal":
            gettingSoundSignal.append(loop_time_ms)
            t_gettingSoundSignal.append(timestamp_ms)

plt.plot(t_effectLoop, effectLoop, label="effectLoop")
plt.plot(t_gettingSoundSignal, gettingSoundSignal, label="gettingSoundSignal")
plt.legend()
plt.ylabel('duration [ms]')
plt.xlabel('runtime [ms]')
plt.title('Time Measurements')
plt.show()
